﻿using CollectionsLINQ.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace CollectionsLINQ
{
    public class MappingService
    {
        private HttpClient _client;
        public static List<Project> projects = new List<Project>();
        public static List<Models.Task> tasks = new List<Models.Task>();
        public static List<Team> teams = new List<Team>();
        public static List<User> users = new List<User>();
        private readonly string _host;

        public MappingService(string host)
        {
            _client = new HttpClient();
            _host = host;
        }

        public async System.Threading.Tasks.Task Init()
        {
            teams = await GetTeams();
            projects = await GetProjects();
            users = await GetUsers();
            tasks = await GetTasks();
        }

        public IEnumerable<Project> GetProjectList()
        {
            var projectsMapping = from project in projects
                                  join user in users on project.AuthorId equals user.Id
                                  join team in teams on project.TeamId equals team.Id
                                  select new Project
                                  {
                                      Name = project.Name,
                                      Description = project.Description,
                                      Deadline = project.Deadline,
                                      CreatedAt = project.CreatedAt,
                                      AuthorId = project.AuthorId,
                                      Author = user,
                                      TeamId = project.TeamId,
                                      Team = team
                                  };

            var tasksMapping =
               from task in tasks
               join user in users on task.PerformerId equals user.Id
               select new Models.Task
               {
                   Id = task.Id,
                   ProjectId = task.ProjectId,
                   PerformerId = task.PerformerId,
                   Performer = user,
                   Name = task.Name,
                   Description = task.Description,
                   State = task.State,
                   CreatedAt = task.CreatedAt,
                   FinishedAt = task.FinishedAt
               };

            var result = projectsMapping.GroupJoin(tasksMapping,
                project => project.Id,
                task => task.ProjectId,
                (project, task) => new Project
                {
                    Name = project.Name,
                    Description = project.Description,
                    Deadline = project.Deadline,
                    CreatedAt = project.CreatedAt,
                    AuthorId = project.AuthorId,
                    Author = project.Author,
                    TeamId = project.TeamId,
                    Team = project.Team,
                    Tasks = task.Select(t => t)
                });

            return result;
        }


        private async Task<List<Models.User>> GetUsers()
        {
            var users = await _client.GetStringAsync(_host + "/api/Users");
            return JsonConvert.DeserializeObject<List<User>>(users);
        }

        private async Task<List<Team>> GetTeams()
        {
            var teams = await _client.GetStringAsync(_host + "/api/Teams");
            return JsonConvert.DeserializeObject<List<Team>>(teams);
        }

        private async Task<List<Models.Task>> GetTasks()
        {
            var teams = await _client.GetStringAsync(_host + "/api/Tasks");
            return JsonConvert.DeserializeObject<List<Models.Task>>(teams);
        }

        private async Task<List<Project>> GetProjects()
        {
            var teams = await _client.GetStringAsync(_host + "/api/Projects");
            return JsonConvert.DeserializeObject<List<Project>>(teams);
        }
    }
}
