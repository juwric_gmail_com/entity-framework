﻿using System;
using System.Collections.Generic;

namespace CollectionsLINQ.Models
{
    public class Project : Base
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }

        public int AuthorId { get; set; }
        public User Author { get; set; }

        public int TeamId { get; set; }
        public Team Team { get; set; }

        public IEnumerable<Task> Tasks { get; set; }
    }
}
