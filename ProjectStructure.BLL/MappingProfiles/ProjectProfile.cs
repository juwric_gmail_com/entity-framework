﻿using AutoMapper;
using ProjectStructure.Common.DTOs.Project;
using ProjectStructure.DAL.Models;

namespace ProjectStructure.BLL.MappingProfiles
{
    public class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<Project, ProjectDTO>();
            CreateMap<ProjectCreateDTO, Project>();
            CreateMap<ProjectUpdateDTO, Project>();
        }
    }
}
