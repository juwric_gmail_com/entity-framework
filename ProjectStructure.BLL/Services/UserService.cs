﻿using AutoMapper;
using ProjectStructure.BLL.Contracts;
using ProjectStructure.Common.DTOs.User;
using ProjectStructure.DAL.Models;
using System;
using System.Collections.Generic;

namespace ProjectStructure.BLL.Services
{
    public class UserService : BaseService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IMapper mapper, IUserRepository userRepository) : base(mapper)
        {
            _userRepository = userRepository;
        }

        public List<UserDTO> GetAll()
        {
            var users = _userRepository.GetAll();
            return _mapper.Map<List<UserDTO>>(users);
        }

        public UserDTO GetUserById(int id)
        {
            var user = _userRepository.GetById(id);
            if (user == null)
            {
                throw new Exception("The user is not found");
            }

            return _mapper.Map<UserDTO>(user);
        }

        public UserDTO CreateUser(UserCreateDTO userCreateDTO)
        {
            var userEntity = _mapper.Map<User>(userCreateDTO);
            //userEntity.Id = (_userRepository.GetAll().Max(u => u.Id)) + 1;
            userEntity.RegisteredAt = DateTime.Now;
            _userRepository.Add(userEntity);

            return _mapper.Map<UserDTO>(userEntity);
        }

        public void UpdateUser(UserUpdateDTO userUpdateDTO)
        {
            var entity = GetUserById(userUpdateDTO.Id);
            var updatedEntity = _mapper.Map<User>(userUpdateDTO);
            updatedEntity.RegisteredAt = entity.RegisteredAt;

            _userRepository.Update(updatedEntity);
        }

        public void DeleteUser(int id)
        {
            GetUserById(id);
            _userRepository.Delete(id);
        }
    }
}
