﻿using ProjectStructure.BLL.Contracts;
using ProjectStructure.DAL;
using ProjectStructure.DAL.Models;

namespace ProjectStructure.BLL.Repositories
{
    public class TeamRepository : BaseRepository<Team>, ITeamRepository
    {
        public TeamRepository(DatabaseContext context) : base(AppDbContext.Teams, context)
        {
        }
    }
}
