﻿using ProjectStructure.BLL.Contracts;
using ProjectStructure.DAL;
using ProjectStructure.DAL.Models;

namespace ProjectStructure.BLL.Repositories
{
    public class ProjectRepository : BaseRepository<Project>, IProjectRepository
    {
        public ProjectRepository(DatabaseContext context) : base(AppDbContext.Projects, context)
        {
        }
    }
}
