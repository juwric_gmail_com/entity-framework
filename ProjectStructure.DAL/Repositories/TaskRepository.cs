﻿using ProjectStructure.BLL.Contracts;
using ProjectStructure.DAL;
using ProjectStructure.DAL.Models;

namespace ProjectStructure.BLL.Repositories
{
    public class TaskRepository : BaseRepository<Task>, ITaskRepository
    {
        public TaskRepository(DatabaseContext context) : base(AppDbContext.Tasks, context)
        {
        }
    }
}
