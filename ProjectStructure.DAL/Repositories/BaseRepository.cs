﻿using Microsoft.EntityFrameworkCore;
using ProjectStructure.BLL.Contracts;
using ProjectStructure.DAL;
using ProjectStructure.DAL.Models;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructure.BLL.Repositories
{
    public class BaseRepository<T> : IBaseRepository<T> where T : BaseEntity
    {
        private readonly List<T> _list;
        private readonly DatabaseContext _context;

        public BaseRepository(List<T> list, DatabaseContext context = null)
        {
            _list = list;
            _context = context;
            _context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }

        public List<T> GetAll()
        {
            if (_context != null)
            {
                return _context.Set<T>().ToList();
            }

            return _list;
        }

        public T GetById(int id)
        {
            if (_context != null)
            {
                return _context.Set<T>().Find(id);
            }

            return _list.FirstOrDefault(e => e.Id == id);
        }

        public void Add(T entity)
        {
            if (_context != null)
            {
                _context.Add(entity);
                _context.SaveChanges();
            }
            else
            {
                _list.Add(entity);
            }
        }

        public void Delete(int id)
        {
            if (_context != null)
            {
                var item = GetById(id);
                if (item != null)
                {
                    _context.Remove(item);
                    _context.SaveChanges();
                }
            }
            else
            {
                var index = _list.FindIndex(e => e.Id == id);
                if (index >= 0)
                {
                    _list.RemoveAt(index);
                }
            }
        }

        public void Update(T entity)
        {
            if (_context != null)
            {
                _context.Entry(entity).State = EntityState.Modified;
                _context.SaveChanges();
            }
            else
            {
                var index = _list.FindIndex(e => e.Id == entity.Id);
                if (index >= 0)
                {
                    _list[index] = entity;
                }
            }
        }
    }
}
