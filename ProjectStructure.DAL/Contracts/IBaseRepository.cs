﻿using System.Collections.Generic;

namespace ProjectStructure.BLL.Contracts
{
    public interface IBaseRepository<T> where T : class
    {
        T GetById(int id);
        List<T> GetAll();

        void Add(T entity);
        void Update(T entity);
        void Delete(int id);
    }
}
