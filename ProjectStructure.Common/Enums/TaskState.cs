﻿namespace ProjectStructure.Common.Enums
{
    public enum TaskState
    {
        Finished,
        UnFinished,
        Canceled,
        InProcess
    }
}
