﻿using System.ComponentModel.DataAnnotations;

namespace ProjectStructure.Common.DTOs.Project
{
    public class ProjectUpdateDTO
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public int AuthorId { get; set; }
        [Required]
        public int TeamId { get; set; }
    }
}
