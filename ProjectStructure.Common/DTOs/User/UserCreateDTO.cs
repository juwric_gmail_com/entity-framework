﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ProjectStructure.Common.DTOs.User
{
    public class UserCreateDTO
    {
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        public DateTime BirthDay { get; set; }

        public int? TeamId { get; set; }
    }
}
