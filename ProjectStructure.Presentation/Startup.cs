using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using ProjectStructure.BLL.MappingProfiles;
using ProjectStructure.Common.Enums;
using ProjectStructure.DAL;
using ProjectStructure.DAL.Models;
using ProjectStructure.Presentation.Extensions;
using ProjectStructure.Presentation.Filters;
using System;
using System.Collections.Generic;

namespace ProjectStructure.Presentation
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers(options => options.Filters.Add(typeof(CustomExceptionFilterAttribute)));
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "ProjectStructure.Presentation", Version = "v1" });
            });

            services.RegisterCustomServices();
            services.AddAutoMapper(typeof(UserProfile));

            services.AddDbContext<DatabaseContext>(options =>
            {
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"));
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "ProjectStructure.Presentation v1"));
            }

            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            //Project Structure Task
            //DbInitializer.Initialize();

            //Entity Framework Task
            InitializeDatabase(app);
        }

        private static void InitializeDatabase(IApplicationBuilder app)
        {
            using (var scope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                using var context = scope.ServiceProvider.GetRequiredService<DatabaseContext>();
                var creator = context.GetService<IDatabaseCreator>() as RelationalDatabaseCreator;
                if (creator.Exists()) { return; }

                context.Database.Migrate();
                var rand = new Random();
                var teams = new List<Team>
                {
                    new Team { Name="Devonte Team", CreatedAt = DateTime.Now },
                    new Team { Name="Anika Team", CreatedAt = DateTime.Now},
                    new Team { Name="Kale Team", CreatedAt = DateTime.Now},
                    new Team { Name="Nyasia Team", CreatedAt = DateTime.Now},
                    new Team { Name="Timmy Team", CreatedAt = DateTime.Now}
                };
                context.Teams.AddRange(teams);
                context.SaveChangesAsync().Wait();

                var users = new List<User>
                {
                    new User { FirstName="Courtney", LastName = "McGlynn", Email="Courtney.McGlynn13@yahoo.com",
                        BirthDay = DateTime.Now.AddYears(-30), RegisteredAt=DateTime.Now,
                        Team = teams[rand.Next(teams.Count)]
                    },

                    new User { FirstName="Danny", LastName = "Bernier", Email="Danny_Bernier56@yahoo.com",
                        BirthDay = DateTime.Now.AddYears(-20), RegisteredAt=DateTime.Now,
                        Team = teams[rand.Next(teams.Count)]
                    },
                    new User { FirstName="Julius", LastName = "Bradtke", Email="Rosemarie55@yahoo.com",
                        BirthDay = DateTime.Now.AddYears(-10), RegisteredAt=DateTime.Now,
                        Team = teams[rand.Next(teams.Count)]
                    },
                    new User { FirstName="Jamie", LastName = "McGStiedemannlynn", Email="Wilma.Stiedemann@hotmail.com",
                        BirthDay = DateTime.Now.AddYears(-50), RegisteredAt=DateTime.Now,
                        Team = teams[rand.Next(teams.Count)]
                    },
                };
                context.Users.AddRange(users);
                context.SaveChangesAsync().Wait();

                var projects = new List<Project>
                {
                    new Project { Name="Arkansas e-tailers PNG", Description="Porro reprehenderit aliquid quo enim enim.",
                        CreatedAt=DateTime.Now, Deadline=DateTime.Now.AddDays(10),
                        Team=teams[rand.Next(teams.Count)], Author=users[rand.Next(users.Count)]
                    },
                    new Project { Name="Cambridgeshire", Description="Ipsam eligendi alias.",
                        CreatedAt=DateTime.Now, Deadline=DateTime.Now.AddDays(10),
                        Team=teams[rand.Next(teams.Count)], Author=users[rand.Next(users.Count)]
                    },
                    new Project { Name="Usability value-added", Description="Porro reprehenderit aliquid quo enim enim.",
                        CreatedAt=DateTime.Now, Deadline=DateTime.Now.AddDays(10),
                        Team=teams[rand.Next(teams.Count)], Author=users[rand.Next(users.Count)]
                    }
                };
                context.Projects.AddRange(projects);
                context.SaveChangesAsync().Wait();

                var tasks = new List<Task>
                {
                    new Task {Name="Human Granite", Description="Ut ut aliquam nihil", State=TaskState.InProcess,
                        CreatedAt=DateTime.Now, FinishedAt=null,
                        Project=projects[rand.Next(projects.Count)], Performer=users[rand.Next(users.Count)]
                    },
                    new Task {Name="Specialist bus", Description="Laboriosam provident officiis", State=TaskState.InProcess,
                        CreatedAt=DateTime.Now, FinishedAt=null,
                        Project=projects[rand.Next(projects.Count)], Performer=users[rand.Next(users.Count)]
                    },
                    new Task {Name="Berkshire invoice", Description="Et ut inventore dolorem et", State=TaskState.InProcess,
                        CreatedAt=DateTime.Now, FinishedAt=null,
                        Project=projects[rand.Next(projects.Count)], Performer=users[rand.Next(users.Count)]
                    }
                };
                context.Tasks.AddRange(tasks);
                context.SaveChangesAsync().Wait();
            };
        }
    }
}
