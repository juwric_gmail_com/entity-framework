﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Services;
using ProjectStructure.Common.DTOs.Team;
using System.Collections.Generic;

namespace ProjectStructure.Presentation.Controllers
{
    public class TeamsController : BaseController
    {
        private readonly TeamService _teamService;

        public TeamsController(TeamService teamService)
        {
            _teamService = teamService;
        }

        [HttpGet]
        public ActionResult<List<TeamDTO>> Get()
        {
            return Ok(_teamService.GetAll());
        }

        [HttpGet("{id}")]
        public ActionResult<TeamDTO> GetById(int id)
        {
            return Ok(_teamService.GetUserById(id));
        }

        [HttpPut]
        public IActionResult UpdateTeam([FromBody] TeamUpdateDTO user)
        {
            _teamService.UpdateUser(user);
            return NoContent();
        }

        [HttpPost]
        public IActionResult CreateTeam([FromBody] TeamCreateDTO user)
        {
            _teamService.CreateUser(user);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _teamService.DeleteUser(id);
            return NoContent();
        }
    }
}
