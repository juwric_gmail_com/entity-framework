﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Services;
using ProjectStructure.Common.DTOs.Project;
using System.Collections.Generic;

namespace ProjectStructure.Presentation.Controllers
{
    public class ProjectsController : BaseController
    {
        private readonly ProjectService _projectService;

        public ProjectsController(ProjectService projectService)
        {
            _projectService = projectService;
        }

        [HttpGet]
        public ActionResult<List<ProjectDTO>> Get()
        {
            return Ok(_projectService.GetAll());
        }

        [HttpGet("{id}")]
        public ActionResult<ProjectDTO> GetById(int id)
        {
            return Ok(_projectService.GetProjectById(id));
        }

        [HttpPut]
        public IActionResult UpdateProject([FromBody] ProjectUpdateDTO project)
        {
            _projectService.UpdateProject(project);
            return NoContent();
        }

        [HttpPost]
        public IActionResult CreateProject([FromBody] ProjectCreateDTO project)
        {
            _projectService.CreateProject(project);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _projectService.DeleteProject(id);
            return NoContent();
        }
    }
}
