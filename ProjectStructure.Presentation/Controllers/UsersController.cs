﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Services;
using ProjectStructure.Common.DTOs.User;
using System.Collections.Generic;

namespace ProjectStructure.Presentation.Controllers
{
    public class UsersController : BaseController
    {
        private readonly UserService _userService;

        public UsersController(UserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        public ActionResult<List<UserDTO>> Get()
        {
            return Ok(_userService.GetAll());
        }

        [HttpGet("{id}")]
        public ActionResult<UserDTO> GetById(int id)
        {
            return Ok(_userService.GetUserById(id));
        }

        [HttpPut]
        public IActionResult UpdateUser([FromBody] UserUpdateDTO user)
        {
            _userService.UpdateUser(user);
            return NoContent();
        }

        [HttpPost]
        public IActionResult CreateUser([FromBody] UserCreateDTO user)
        {
            _userService.CreateUser(user);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _userService.DeleteUser(id);
            return NoContent();
        }
    }
}
